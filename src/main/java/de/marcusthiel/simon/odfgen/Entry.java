package de.marcusthiel.simon.odfgen;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.Charset;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.eclipse.jetty.apache.jsp.JettyJasperInitializer;
import org.eclipse.jetty.jsp.JettyJspServlet;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.h2.jdbcx.JdbcDataSource;

import java.nio.file.Paths;

public class Entry extends JFrame {

	private static final long serialVersionUID = -6768398597631067638L;
	private Server server;
	private QueryRunner queryRunner;

	public Entry() {
		super("Simon Transporte: Angebote");
		setLocation(300, 300);
		setSize(300, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout(5, 5));

		JPanel panel = new JPanel(new GridLayout(2, 1));
		JButton button2 = new JButton("Link Öffnen");
		button2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().browse(new URI("http://localhost:8080/"));
				} catch (IOException | URISyntaxException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		});

		JButton button = new JButton("Ende!");
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					server.stop();
					server.destroy();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.exit(0);
			}
		});

		panel.add(button2);
		panel.add(button);

		getContentPane().add(panel);

		setVisible(true);
	}

	public static void main(String[] args) {

		try {

			// Desktop.getDesktop().browse(new URI("http://localhost:8080/"));
			Entry me = new Entry();
			me.initDB();
			me.startServer();

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}

		System.exit(0);
	}

	private void startServer() throws Exception {
		Server server = new Server(8080);

		ContextHandler contextHandler = new ContextHandler("/");
		String htmlbase = new File("html").toURI().toASCIIString();
		contextHandler.setResourceBase("html");
		ResourceHandler resourceHandler = new ResourceHandler();
		contextHandler.setHandler(resourceHandler);

		ServletContextHandler work = new ServletContextHandler(ServletContextHandler.SESSIONS);
		work.setContextPath("/servlet");
		Worker worker = new Worker(queryRunner);
		work.addServlet(new ServletHolder(worker), "/work/*");

		// Create Servlet context
		ServletContextHandler jspContextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
		jspContextHandler.setContextPath("/jsp/");
		jspContextHandler.setResourceBase(htmlbase);
		// Since this is a ServletContextHandler we must manually configure JSP support.
		enableEmbeddedJspSupport(jspContextHandler);

		ServletContextHandler preprozessor = new ServletContextHandler(ServletContextHandler.SESSIONS);
		preprozessor.setContextPath("/");
		JspPreprozessor jspPreprozessor = new JspPreprozessor(queryRunner,jspContextHandler);
		preprozessor.addServlet(new ServletHolder(jspPreprozessor), "/");

		ContextHandlerCollection contexts = new ContextHandlerCollection();
		contexts.setHandlers(new Handler[] {  preprozessor, work, jspContextHandler, resourceHandler });

		server.setHandler(contexts);
		server.setStopAtShutdown(true);

		server.start();

		this.server = server;
		server.join();

	}

	private static void enableEmbeddedJspSupport(ServletContextHandler servletContextHandler) throws IOException {
		// Establish Scratch directory for the servlet context (used by JSP compilation)
		File tempDir = new File(System.getProperty("java.io.tmpdir"));
		File scratchDir = new File(tempDir.toString(), "embedded-jetty-jsp");

		if (!scratchDir.exists()) {
			if (!scratchDir.mkdirs()) {
				throw new IOException("Unable to create scratch directory: " + scratchDir);
			}
		}
		servletContextHandler.setAttribute("javax.servlet.context.tempdir", scratchDir);

		// Set Classloader of Context to be sane (needed for JSTL)
		// JSP requires a non-System classloader, this simply wraps the
		// embedded System classloader in a way that makes it suitable
		// for JSP to use
		ClassLoader jspClassLoader = new URLClassLoader(new URL[0], Entry.class.getClassLoader());
		servletContextHandler.setClassLoader(jspClassLoader);

		// Manually call JettyJasperInitializer on context startup
		servletContextHandler.addBean(new JspStarter(servletContextHandler));

		// Create / Register JSP Servlet (must be named "jsp" per spec)
		ServletHolder holderJsp = new ServletHolder("jsp", JettyJspServlet.class);
		holderJsp.setInitOrder(0);
		holderJsp.setInitParameter("logVerbosityLevel", "DEBUG");
		holderJsp.setInitParameter("fork", "false");
		holderJsp.setInitParameter("xpoweredBy", "false");
		holderJsp.setInitParameter("compilerTargetVM", "1.8");
		holderJsp.setInitParameter("compilerSourceVM", "1.8");
		holderJsp.setInitParameter("keepgenerated", "true");
		servletContextHandler.addServlet(holderJsp, "*.jsp");
	}

	private void initDB() {
		try {
			Class.forName("org.h2.Driver");

			JdbcDataSource ds = new JdbcDataSource();
			ds.setURL("jdbc:h2:./data/simon");

			QueryRunner qr = new QueryRunner(ds);

			InputStream sqlStream = getClass().getResourceAsStream("/de/marcusthiel/simon/odfgen/h2Database.sql");

			for (String s : IOUtils.toString(sqlStream, Charset.forName("UTF-8")).split(";")) {
				qr.execute(s);
			}

			this.queryRunner = qr;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public static class JspStarter extends AbstractLifeCycle
			implements ServletContextHandler.ServletContainerInitializerCaller {
		JettyJasperInitializer sci;
		ServletContextHandler context;

		public JspStarter(ServletContextHandler context) {
			this.sci = new JettyJasperInitializer();
			this.context = context;
			this.context.setAttribute("org.apache.tomcat.JarScanner", new StandardJarScanner());
		}

		@Override
		protected void doStart() throws Exception {
			ClassLoader old = Thread.currentThread().getContextClassLoader();
			Thread.currentThread().setContextClassLoader(context.getClassLoader());
			try {
				sci.onStartup(null, context.getServletContext());
				super.doStart();
			} finally {
				Thread.currentThread().setContextClassLoader(old);
			}
		}
	}
}
