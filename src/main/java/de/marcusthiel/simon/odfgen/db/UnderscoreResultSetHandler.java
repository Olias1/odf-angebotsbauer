package de.marcusthiel.simon.odfgen.db;

import java.beans.PropertyDescriptor;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class UnderscoreResultSetHandler<T> extends BeanListHandler<T> {

	public UnderscoreResultSetHandler(Class<? extends T> type) {
		super(type,new BasicRowProcessor(new MyBeanProcessor()));
	}
	
	
	static class MyBeanProcessor extends BeanProcessor{
		@Override
		protected int[] mapColumnsToProperties(ResultSetMetaData rsmd,
	            PropertyDescriptor[] props) throws SQLException {

	        int cols = rsmd.getColumnCount();
	        int[] columnToProperty = new int[cols + 1];
	        Arrays.fill(columnToProperty, PROPERTY_NOT_FOUND);

	        for (int col = 1; col <= cols; col++) {
	            String columnName = rsmd.getColumnLabel(col);
	            if (null == columnName || 0 == columnName.length()) {
	              columnName = rsmd.getColumnName(col);
	            }
	            for (int i = 0; i < props.length; i++) {

	                if (columnName.equalsIgnoreCase(props[i].getName())) {
	                    columnToProperty[col] = i;
	                    break;
	                }
	                if (columnName.replace("_", "").equalsIgnoreCase(props[i].getName())) {
	                    columnToProperty[col] = i;
	                    break;
	                }
	            }
	        }

	        return columnToProperty;
	    }
	}


}
