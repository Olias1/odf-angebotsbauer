package de.marcusthiel.simon.odfgen.db.data;

import java.util.Date;

public class CallData {
	
	private Long id;
	private long callId;
	private String fieldName;
	private String value;
	private Date tsLastUsage;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public long getCallId() {
		return callId;
	}
	public void setCallId(long callId) {
		this.callId = callId;
	}
	public Date getTsLastUsage() {
		return tsLastUsage;
	}
	public void setTsLastUsage(Date tsLastUsage) {
		this.tsLastUsage = tsLastUsage;
	}
	
	

}
