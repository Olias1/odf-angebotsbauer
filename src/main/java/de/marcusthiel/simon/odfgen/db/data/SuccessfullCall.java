package de.marcusthiel.simon.odfgen.db.data;

import java.util.Date;

public class SuccessfullCall {

	private Long id;
	private Date tsLastUsage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTsLastUsage() {
		return tsLastUsage;
	}

	public void setTsLastUsage(Date tsLastUsage) {
		this.tsLastUsage = tsLastUsage;
	}

}
