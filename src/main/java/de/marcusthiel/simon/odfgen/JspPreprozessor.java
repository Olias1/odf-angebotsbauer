package de.marcusthiel.simon.odfgen;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.dbutils.QueryRunner;
import org.eclipse.jetty.http.HttpURI;
import org.eclipse.jetty.server.Dispatcher;
import org.eclipse.jetty.server.handler.ContextHandler;

import de.marcusthiel.simon.odfgen.db.UnderscoreResultSetHandler;
import de.marcusthiel.simon.odfgen.db.data.CallData;

public class JspPreprozessor extends HttpServlet {

	private QueryRunner queryRunner;
	private ContextHandler jspContext;

	public JspPreprozessor(QueryRunner queryRunner, ContextHandler jspContext) {
		this.queryRunner = queryRunner;
		this.jspContext = jspContext;
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = new Dispatcher(jspContext, new HttpURI(request.getRequestURI()), "/index.jsp");

		try {
			try {
				Long oldID = Long.valueOf(request.getParameter("oldData"));
				List<CallData> data = queryRunner.query("SELECT FIELD_NAME, VALUE FROM CallData where CALL_ID = ?",
						new UnderscoreResultSetHandler<CallData>(CallData.class), oldID);
				HashMap<String, String> map = new HashMap<>();
				for (CallData d : data) {
					map.put(d.getFieldName(), d.getValue());
				}
				System.out.println(map.get("from_floor"));
				request.setAttribute("loadData", map);

			} catch (Exception e) {
			}

			List<CallData> data = queryRunner.query(
					"SELECT cd.*, TS_LAST_USAGE FROM CallData cd "
							+ " left join SuccessfullCall sc on sc.ID = cd.CALL_ID"
							+ " where cd.FIELD_NAME in ( ? , ? ) order by TS_LAST_USAGE ASC",
					new UnderscoreResultSetHandler<CallData>(CallData.class), "lstName", "fstName");

			HashMap<Long, String> map = new HashMap<>();
			CallData fst = null, lst = null;
			Long lastId = null;
			for (CallData d : data) {
				if (lastId == null)
					lastId = d.getCallId();

				if (lst != null && lastId != d.getCallId()) {
					map.put(lastId, formatOption(fst, lst));
					lastId = d.getCallId();
					fst = lst = null;
				}

				if (d.getFieldName().equalsIgnoreCase("lstName")) {
					lst = d;
				}
				if (d.getFieldName().equalsIgnoreCase("fstName")) {
					fst = d;
				}
			}

			if (lst != null && lastId != null) {
				map.put(lastId, formatOption(fst, lst));
			}

			List<Map.Entry<Long, String>> list = new ArrayList(map.entrySet());
			Collections.sort(list, new Comparator<Map.Entry<Long, String>>() {

				@Override
				public int compare(Entry<Long, String> o1, Entry<Long, String> o2) {
					return (int)(o2.getKey() - o1.getKey());
				}
			});
			request.setAttribute("callData", list);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rd.forward(request, response);
	}

	private String formatOption(CallData fst, CallData lst) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		String l = lst.getValue(), f;
		f = (fst != null) ? f = "," + fst.getValue() : "";
		String d = sdf.format(lst.getTsLastUsage());
		return d+"&nbsp;&nbsp;&nbsp;"+l + f;
	}
}
