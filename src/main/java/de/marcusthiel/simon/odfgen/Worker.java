package de.marcusthiel.simon.odfgen;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.event.ConfigurationEvent;
import org.apache.commons.configuration.event.ConfigurationListener;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.xerces.dom.TextImpl;
import org.odftoolkit.odfdom.dom.OdfSchemaDocument;
import org.odftoolkit.odfdom.dom.attribute.draw.DrawNameAttribute;
import org.odftoolkit.odfdom.dom.attribute.draw.DrawStyleNameAttribute;
import org.odftoolkit.odfdom.dom.attribute.fo.FoFontFamilyAttribute;
import org.odftoolkit.odfdom.dom.attribute.fo.FoFontSizeAttribute;
import org.odftoolkit.odfdom.dom.attribute.fo.FoMarginTopAttribute;
import org.odftoolkit.odfdom.dom.attribute.fo.FoMinHeightAttribute;
import org.odftoolkit.odfdom.dom.attribute.style.StyleNameAttribute;
import org.odftoolkit.odfdom.dom.attribute.style.StylePageLayoutNameAttribute;
import org.odftoolkit.odfdom.dom.attribute.style.StyleTextUnderlineColorAttribute;
import org.odftoolkit.odfdom.dom.attribute.style.StyleTextUnderlineStyleAttribute;
import org.odftoolkit.odfdom.dom.attribute.style.StyleTextUnderlineWidthAttribute;
import org.odftoolkit.odfdom.dom.attribute.svg.SvgStrokeColorAttribute;
import org.odftoolkit.odfdom.dom.attribute.svg.SvgX1Attribute;
import org.odftoolkit.odfdom.dom.attribute.svg.SvgX2Attribute;
import org.odftoolkit.odfdom.dom.attribute.svg.SvgY1Attribute;
import org.odftoolkit.odfdom.dom.attribute.svg.SvgY2Attribute;
import org.odftoolkit.odfdom.dom.attribute.text.TextAnchorTypeAttribute;
import org.odftoolkit.odfdom.dom.attribute.text.TextStyleNameAttribute;
import org.odftoolkit.odfdom.dom.element.draw.DrawLineElement;
import org.odftoolkit.odfdom.dom.element.office.OfficeTextElement;
import org.odftoolkit.odfdom.dom.element.style.StyleFooterElement;
import org.odftoolkit.odfdom.dom.element.style.StyleGraphicPropertiesElement;
import org.odftoolkit.odfdom.dom.element.style.StyleHeaderFooterPropertiesElement;
import org.odftoolkit.odfdom.dom.element.style.StyleMasterPageElement;
import org.odftoolkit.odfdom.dom.element.style.StylePageLayoutElement;
import org.odftoolkit.odfdom.dom.element.style.StyleParagraphPropertiesElement;
import org.odftoolkit.odfdom.dom.element.style.StyleTabStopElement;
import org.odftoolkit.odfdom.dom.element.style.StyleTabStopsElement;
import org.odftoolkit.odfdom.dom.element.style.StyleTextPropertiesElement;
import org.odftoolkit.odfdom.dom.element.text.TextListLevelStyleBulletElement;
import org.odftoolkit.odfdom.dom.element.text.TextPElement;
import org.odftoolkit.odfdom.dom.element.text.TextTabElement;
import org.odftoolkit.odfdom.dom.style.OdfStyleFamily;
import org.odftoolkit.odfdom.incubator.doc.style.OdfStyle;
import org.odftoolkit.odfdom.incubator.doc.style.OdfStylePageLayout;
import org.odftoolkit.odfdom.incubator.doc.text.OdfTextListStyle;
import org.odftoolkit.odfdom.pkg.OdfElement;
import org.odftoolkit.odfdom.pkg.OdfFileDom;
import org.odftoolkit.odfdom.pkg.OdfName;
import org.odftoolkit.odfdom.pkg.OdfXMLFactory;
import org.odftoolkit.odfdom.type.Color;
import org.odftoolkit.simple.Document;
import org.odftoolkit.simple.TextDocument;
import org.odftoolkit.simple.draw.FrameRectangle;
import org.odftoolkit.simple.draw.Textbox;
import org.odftoolkit.simple.style.Font;
import org.odftoolkit.simple.style.PageLayoutProperties;
import org.odftoolkit.simple.style.ParagraphProperties;
import org.odftoolkit.simple.style.StyleTypeDefinitions;
import org.odftoolkit.simple.style.StyleTypeDefinitions.FontStyle;
import org.odftoolkit.simple.style.StyleTypeDefinitions.HorizontalAlignmentType;
import org.odftoolkit.simple.style.StyleTypeDefinitions.SupportedLinearMeasure;
import org.odftoolkit.simple.style.TextProperties;
import org.odftoolkit.simple.text.Footer;
import org.odftoolkit.simple.text.Paragraph;
import org.odftoolkit.simple.text.list.ListItem;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import de.marcusthiel.simon.odfgen.db.UnderscoreResultSetHandler;
import de.marcusthiel.simon.odfgen.db.data.CallData;
import de.marcusthiel.simon.odfgen.db.data.SuccessfullCall;


public class Worker extends HttpServlet implements ConfigurationListener {

	private XMLConfiguration config;

	private static final long serialVersionUID = 4749354281780950175L;
	private static final boolean debug = false;

	public final String STREET_FROM_TOKEN = "%%AUFL_STRASSE%%";
	public final String CITY_FROM_TOKEN = "%%AUFL_CITY%%";
	public final String ZIP_FROM_TOKEN = "%%AUFL_PLZ%%";

	public final String STREET_TO_TOKEN = "%%ABL_STRASSE%%";
	public final String CITY_TO_TOKEN = "%%ABL_CITY%%";
	public final String ZIP_TO_TOKEN = "%%ABL_PLZ%%";

	public final String SUM_TOKEN = "%%SUM%%";

	private final static Font arial12 = getArialFont(12, false, false);
	private final static Font arial12Bold = getArialFont(12, true, false);
	private final static Font arial13Bold = getArialFont(13, true, false);
	private final static Font arial14Bold = getArialFont(14, true, false);

	private BigDecimal nettoSum = BigDecimal.ZERO;
	private BigDecimal taxSum = BigDecimal.ZERO;
	private BigDecimal totalSum = BigDecimal.ZERO;
	private DecimalFormat df = new DecimalFormat("0.00");
	private final static char EURO_SIGN = '\u20AC';

	private QueryRunner queryRunner;

	public Worker(QueryRunner queryRunner) {
		try {
			this.queryRunner= queryRunner;
			this.config = new XMLConfiguration();
			config.setDelimiterParsingDisabled(true);
			config.load("./config/config.xml");
			config.setReloadingStrategy(new FileChangedReloadingStrategy());
			config.addConfigurationListener(this);
		} catch (ConfigurationException e) {
			throw new RuntimeException(e);
		}
	}

	public QueryRunner getQueryRunner() {
		return queryRunner;
	}

	public void setQueryRunner(QueryRunner queryRunner) {
		this.queryRunner = queryRunner;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Map<String, String> map = new HashMap<String, String>();

		for (Entry<String, String[]> para : req.getParameterMap().entrySet()) {
			if (para.getValue().length == 0) {
				continue;
			}
			String v = para.getValue()[0];
			if (StringUtils.isEmpty(v)) {
				continue;
			}

			map.put(para.getKey(), v.trim());
		}

		nettoSum = BigDecimal.ZERO;
		try {
			Document odf = buildODF(map);
			resp.setHeader("Content-Type", "application/vnd.oasis.opendocument.text");
			String fname = new SimpleDateFormat("yyy-MM-dd").format(new Date()) + '_'
					+ map.get("lstName").replaceAll("\\s+", "");

			resp.setHeader("Content-Disposition", "inline;filename=\"" + fname + "\"");
			odf.save(resp.getOutputStream());
		} catch (Exception e) {
			PrintWriter pw = new PrintWriter(resp.getOutputStream());
			pw.print("Da ist was schiefgegangen...\r\n-------\r\n\r\n");
			e.printStackTrace(pw);
			resp.setHeader("Content-Type", "text/plain");
			resp.setStatus(500);
			pw.flush();
			e.printStackTrace();
		}
	}

	public static Font getArialFont(int size, boolean bold, boolean italic) {

		FontStyle style = StyleTypeDefinitions.FontStyle.REGULAR;
		if (bold)
			style = StyleTypeDefinitions.FontStyle.BOLD;
		if (italic)
			style = StyleTypeDefinitions.FontStyle.ITALIC;
		return new Font("Arial", style, size, Color.BLACK);
	}

	private Document buildODF(Map<String, String> map) throws Exception {
		TextDocument odt;
		try {

			odt = TextDocument.newTextDocument();

			TextPElement te;
			do {
				te = OdfElement.findFirstChildNode(TextPElement.class, odt.getContentRoot());
				if (te == null) {
					break;
				}
				odt.getContentRoot().removeChild(te);
			} while (true);
			addfoldmarks(odt);
			setPageFormat(odt);

			List<String> postAdr = getFROMAddress(map);

			Paragraph rootP = odt.addParagraph(null);

			// A4 = 210 breit, 20 mm rand, netto 17cm
			// 297 hoch, netto 257
			Textbox adressTB = rootP.addTextbox(new FrameRectangle(0.5, 2.5, 15, 3, SupportedLinearMeasure.CM));

			Paragraph fromLine = adressTB.addParagraph(config.getString("fromLine") + "\r\n");
			te = (TextPElement) fromLine.getOdfElement();
			OdfStyle fromLineStyle = getStyleFromLine(odt);
			setAttribute(te, TextStyleNameAttribute.ATTRIBUTE_NAME, fromLineStyle.getStyleNameAttribute());

			Paragraph adressP = adressTB.addParagraph(StringUtils.join(postAdr, "\r\n"));
			if (debug)
				adressTB.setBackgroundColor(Color.GRAY);

			adressP.setFont(arial12);

			rootP.getStyleHandler().getParagraphPropertiesForWrite().setMarginTop(58);
			String loc = config.getString("city");
			Paragraph dateP = odt
					.addParagraph(loc + new SimpleDateFormat(", 'den' dd.MM.yyyy", Locale.GERMAN).format(new Date()));
			dateP.setFont(arial12);
			dateP.setHorizontalAlignment(HorizontalAlignmentType.RIGHT);

			Paragraph subj = odt.addParagraph(getSubjectLine(map));
			subj.setFont(arial13Bold);

			odt.addParagraph(null); // blank
			odt.addParagraph(getSalutation(map, false)).setFont(arial12);
			odt.addParagraph(null); // blank
			Paragraph textpara = odt.addParagraph(getText(map));
			textpara.setFont(arial12);
			org.odftoolkit.simple.text.list.List list = odt.addList();
			OdfTextListStyle listStyle = getListStyle(odt);
			setAttribute(list.getOdfElement(), StyleNameAttribute.ATTRIBUTE_NAME, listStyle.getStyleNameAttribute());

			OdfStyle tabStyle = getTabStyle(odt);

			for (String itm : getList(map)) {
				try {
					ListItem item = list.addItem(itm);
					te = OdfElement.findFirstChildNode(TextPElement.class, item.getOdfElement());
					setAttribute(te, TextStyleNameAttribute.ATTRIBUTE_NAME, tabStyle.getStyleNameAttribute());
					replaceTabs(odt.getContentDom(), (Text) te.getChildNodes().item(0));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			taxSum = new BigDecimal(0.19).multiply(nettoSum).setScale(2, RoundingMode.HALF_UP);
			totalSum = nettoSum.add(taxSum).setScale(2, RoundingMode.HALF_UP);

			String sumToken = config.getString("list.sumToken[@token]");
			String newText = textpara.getTextContent().replace(sumToken, df.format(totalSum) + " " + EURO_SIGN);
			textpara.setTextContent(newText);

			Paragraph taxline2 = odt.addParagraph("\tzzgl. 19% Mwst " + df.format(taxSum) + " " + EURO_SIGN);
			te = (TextPElement) taxline2.getOdfElement();
			setAttribute(te, TextStyleNameAttribute.ATTRIBUTE_NAME, tabStyle.getStyleNameAttribute());

			Paragraph taxline1 = odt.addParagraph(
					"\t\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500");
			te = (TextPElement) taxline1.getOdfElement();
			setAttribute(te, TextStyleNameAttribute.ATTRIBUTE_NAME, tabStyle.getStyleNameAttribute());

			OdfStyle tabstyle2 = getTabStyle(odt);
			TextProperties textProp = TextProperties.getOrCreateTextProperties(tabstyle2);
			textProp.setFont(arial14Bold);
			Paragraph totalLine = odt.addParagraph("\tEndsumme " + df.format(totalSum) + " " + EURO_SIGN);
			te = (TextPElement) totalLine.getOdfElement();
			setAttribute(te, TextStyleNameAttribute.ATTRIBUTE_NAME, tabstyle2.getStyleNameAttribute());

			buildEndtext(odt, map);

			saveMap(map);

			return odt;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void saveMap(Map<String, String> map) {
		if (queryRunner == null)
			return;

		try {

			SuccessfullCall sc = new SuccessfullCall();

			sc.setTsLastUsage(new Date());
			String sql = "SELECT ID id,CALL_ID callId, FIELD_NAME fieldName, VALUE value  FROM CallData WHERE FIELD_NAME = ? and VALUE = ?";
			List<CallData> callD = queryRunner.query(sql, new BeanListHandler<CallData>(CallData.class),  "lstName", map.get("lstName") );
			
			Long callId = null;
			
			if (!callD.isEmpty()) {
				callId = callD.get(0).getCallId();
				queryRunner.update("UPDATE SuccessfullCall set TS_LAST_USAGE = ? where ID = ?", new Date(), callId);
				
				queryRunner.execute("DELETE FROM CallData WHERE CALL_ID=?", callId);
			}else {
				callId = queryRunner.insert("INSERT INTO SuccessfullCall (TS_LAST_USAGE) VALUES (?) ", new ScalarHandler<Long>(), new Date());
			}
			
			for (Entry<String, String> entry : map.entrySet()) {		
				queryRunner.execute("INSERT INTO CallData (CALL_ID,FIELD_NAME,VALUE) VALUES (?,?,?) ", callId, entry.getKey(), entry.getValue());
			}
			
			queryRunner.getDataSource().getConnection().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void addfoldmarks(TextDocument odt) throws Exception {
		OdfStyle style = odt.getContentDom().getAutomaticStyles().newStyle(OdfStyleFamily.Graphic);
		setAttribute(style, StyleNameAttribute.ATTRIBUTE_NAME, RandomStringUtils.randomAlphanumeric(8));

		StyleGraphicPropertiesElement props = new StyleGraphicPropertiesElement(odt.getContentDom());
		setAttribute(props, SvgStrokeColorAttribute.ATTRIBUTE_NAME, Color.GRAY);
		style.appendChild(props);

		OfficeTextElement rootele = odt.getContentRoot();
		DrawLineElement line;

		/*
		 * line = new DrawLineElement(odt.getContentDom()); setAttribute(line,
		 * TextAnchorTypeAttribute.ATTRIBUTE_NAME, TextAnchorTypeAttribute.Value.PAGE);
		 * setAttribute(line, DrawNameAttribute.ATTRIBUTE_NAME, "FOLDMARK_B_TOP");
		 * setAttribute(line, DrawStyleNameAttribute.ATTRIBUTE_NAME,
		 * style.getStyleNameAttribute()); setAttribute(line,
		 * SvgY1Attribute.ATTRIBUTE_NAME, "10.5cm"); setAttribute(line,
		 * SvgY2Attribute.ATTRIBUTE_NAME, "10.5cm");
		 * 
		 * setAttribute(line, SvgX1Attribute.ATTRIBUTE_NAME, "0cm"); setAttribute(line,
		 * SvgX2Attribute.ATTRIBUTE_NAME, "0.8cm"); rootele.appendChild(line);
		 * 
		 * line = new DrawLineElement(odt.getContentDom()); setAttribute(line,
		 * DrawStyleNameAttribute.ATTRIBUTE_NAME, style.getStyleNameAttribute());
		 * setAttribute(line, DrawNameAttribute.ATTRIBUTE_NAME, "FOLDMARK_B_BUTTOM");
		 * setAttribute(line, TextAnchorTypeAttribute.ATTRIBUTE_NAME,
		 * TextAnchorTypeAttribute.Value.PAGE); setAttribute(line,
		 * SvgY1Attribute.ATTRIBUTE_NAME, "21cm"); setAttribute(line,
		 * SvgY2Attribute.ATTRIBUTE_NAME, "21cm");
		 * 
		 * setAttribute(line, SvgX1Attribute.ATTRIBUTE_NAME, "0cm"); setAttribute(line,
		 * SvgX2Attribute.ATTRIBUTE_NAME, "0.8cm"); rootele.appendChild(line);
		 */
		line = new DrawLineElement(odt.getContentDom());
		setAttribute(line, DrawStyleNameAttribute.ATTRIBUTE_NAME, style.getStyleNameAttribute());
		setAttribute(line, DrawNameAttribute.ATTRIBUTE_NAME, "FOLDMARK_Middle");
		setAttribute(line, TextAnchorTypeAttribute.ATTRIBUTE_NAME, TextAnchorTypeAttribute.Value.PAGE);
		setAttribute(line, SvgY1Attribute.ATTRIBUTE_NAME, "14.85cm");
		setAttribute(line, SvgY2Attribute.ATTRIBUTE_NAME, "14.85cm");

		setAttribute(line, SvgX1Attribute.ATTRIBUTE_NAME, "0cm");
		setAttribute(line, SvgX2Attribute.ATTRIBUTE_NAME, "0.4cm");
		rootele.appendChild(line);

	}

	private void setAttribute(Element ele, OdfName name, Object val) {
		ele.setAttributeNS(name.getUri(), name.getQName(), val.toString());
	}

	private void buildEndtext(TextDocument odt, Map<String, String> map) throws Exception {
		config.setDelimiterParsingDisabled(false);
		Paragraph para = null;
		for (HierarchicalConfiguration p : config.configurationsAt("endtext.p")) {
			boolean ifOk = true;
			String[] ifFs = p.getStringArray("[@if]");
			if (ifFs.length == 1)
				ifFs = ifFs[0].split(",");
			for (String ifF : ifFs) {
				String v = map.get(ifF);
				if (ifF == null && StringUtils.isBlank(v) && v.equalsIgnoreCase("0")) {
					ifOk = false;
				}
			}
			if (!ifOk)
				continue;

			ifFs = p.getStringArray("[@ifnot]");
			if (ifFs.length == 1)
				ifFs = ifFs[0].split(",");
			for (String ifF : ifFs) {
				String v = map.get(ifF);
				if (ifF != null && StringUtils.isNotBlank(v) && !v.equalsIgnoreCase("0")) {
					ifOk = false;
				}
			}
			if (!ifOk)
				continue;

			String t = p.getString(".");
			if (p.containsKey("[@strip]")) {
				t = t.replaceAll("\\s+", " ").trim();
			}
			if (p.containsKey("[@continue]") && para != null) {
				t = para.getOdfElement().getFirstChild().getNodeValue() + " " + t;
				para.getOdfElement().getFirstChild().setNodeValue(t);
			} else {
				odt.addParagraph(null);
				para = odt.addParagraph(t);
			}

			if (p.containsKey("[@bold]")) {
				para.setFont(arial12Bold);
			} else {
				para.setFont(arial12);
			}
		}

		odt.addParagraph(null);
		Footer footer = odt.getFooter(true);

		StyleFooterElement sfe = footer.getOdfElement();
		// der Footer ist im StyleDom definiert!!
		Element masterpage = (Element) sfe.getParentNode();
		NamedNodeMap atrList = masterpage.getAttributes();
		while (atrList.getLength() != 0) {
			Attr atr = (Attr) atrList.item(0);
			masterpage.removeAttributeNode(atr);
		}
		setAttribute(masterpage, StyleNameAttribute.ATTRIBUTE_NAME, "Standard"); // nicht fragen...

		StylePageLayoutElement pagelayout = new StylePageLayoutElement(odt.getStylesDom());
		setAttribute(pagelayout, StyleNameAttribute.ATTRIBUTE_NAME, RandomStringUtils.randomAlphanumeric(5));
		StyleHeaderFooterPropertiesElement footerProps = pagelayout.newStyleFooterStyleElement()
				.newStyleHeaderFooterPropertiesElement();
		setAttribute(footerProps, FoMinHeightAttribute.ATTRIBUTE_NAME, "0cm");
		setAttribute(footerProps, FoMarginTopAttribute.ATTRIBUTE_NAME, "0.5cm");
		setAttribute(masterpage, StylePageLayoutNameAttribute.ATTRIBUTE_NAME, pagelayout.getStyleNameAttribute());

		odt.getStylesDom().getAutomaticStyles().appendChild(pagelayout);

		OdfStyle fontstyle = getFooterFontStyle(odt);

		for (HierarchicalConfiguration p : config.configurationsAt("foot")) {
			TextPElement pe = new TextPElement(odt.getStylesDom());

			String t = p.getString(".");
			if (p.containsKey("[@strip]")) {
				t = t.replaceAll("\\s+", " ").trim();
			}
			TextImpl dt = new org.apache.xerces.dom.TextImpl(odt.getStylesDom(), t);
			pe.appendChild(dt);
			sfe.appendChild(pe);

			setAttribute(pe, TextStyleNameAttribute.ATTRIBUTE_NAME, fontstyle.getStyleNameAttribute());
		}

	}

	private OdfStyle getFooterFontStyle(TextDocument odt) throws Exception {
		OdfStyle style = odt.getStylesDom().getAutomaticStyles().newStyle(OdfStyleFamily.Paragraph);
		style.setStyleNameAttribute(RandomStringUtils.randomAlphanumeric(4));

		TextProperties textProp = TextProperties.getOrCreateTextProperties(style);
		textProp.setFont(getArialFont(10, false, false));

		return style;
	}

	private OdfStyle getRightStyle(OdfSchemaDocument odt) throws Exception {
		OdfStyle style = odt.getOrCreateDocumentStyles().newStyle(RandomStringUtils.randomAlphanumeric(8),
				OdfStyleFamily.Paragraph);

		ParagraphProperties paraProps = ParagraphProperties.getOrCreateParagraphProperties(style);
		paraProps.setHorizontalAlignment(HorizontalAlignmentType.RIGHT);

		TextProperties textProp = TextProperties.getOrCreateTextProperties(style);
		textProp.setFont(arial12);

		return style;
	}

	private OdfTextListStyle getListStyle(OdfSchemaDocument odt) throws Exception {
		OdfTextListStyle style = odt.getContentDom().getAutomaticStyles().newListStyle();
		style.setStyleNameAttribute(RandomStringUtils.randomAlphanumeric(4));

		for (int i = 0; i < 5; i++) {
			TextListLevelStyleBulletElement lvl = (TextListLevelStyleBulletElement) style.getOrCreateListLevel(i,
					TextListLevelStyleBulletElement.class);
			lvl.setTextBulletCharAttribute("\u2022");
		}
		return style;

	}

	private OdfStyle getStyleFromLine(OdfSchemaDocument odf) throws Exception {
		OdfStyle style = odf.getContentDom().getAutomaticStyles().newStyle(OdfStyleFamily.Paragraph);
		style.setStyleNameAttribute("LL_" + RandomStringUtils.randomAlphanumeric(3));

		OdfFileDom styledom = odf.getContentDom();
		StyleTextPropertiesElement textProp = new StyleTextPropertiesElement(styledom);
		setAttribute(textProp, FoFontFamilyAttribute.ATTRIBUTE_NAME, "Arial");
		setAttribute(textProp, FoFontSizeAttribute.ATTRIBUTE_NAME, "6");
		setAttribute(textProp, StyleTextUnderlineStyleAttribute.ATTRIBUTE_NAME,
				StyleTextUnderlineStyleAttribute.Value.SOLID);
		setAttribute(textProp, StyleTextUnderlineWidthAttribute.ATTRIBUTE_NAME,
				StyleTextUnderlineWidthAttribute.Value.AUTO);
		setAttribute(textProp, StyleTextUnderlineColorAttribute.ATTRIBUTE_NAME,
				StyleTextUnderlineColorAttribute.Value.FONT_COLOR);
		style.appendChild(textProp);

		return style;
	}

	private OdfStyle getTabStyle(OdfSchemaDocument odt) throws Exception {

		OdfStyle style = odt.getOrCreateDocumentStyles().newStyle(RandomStringUtils.randomAlphanumeric(8),
				OdfStyleFamily.Paragraph);

		OdfFileDom styledom = odt.getStylesDom();
		StyleTabStopsElement stops = new StyleTabStopsElement(styledom);
		StyleTabStopElement stop = new StyleTabStopElement(styledom);
		stop.setStyleTypeAttribute("right");
		String tabRight = config.getString("listtabRight", "17cm");
		stop.setStylePositionAttribute(tabRight);

		StyleParagraphPropertiesElement paraProps = style.newStyleParagraphPropertiesElement();
		paraProps.appendChild(stops);
		stops.appendChild(stop);

		TextProperties textProp = TextProperties.getOrCreateTextProperties(style);
		textProp.setFont(arial12);

		return style;
	}

	private List<String> getList(Map<String, String> map) {
		List<String> result = new ArrayList<>();
		List<HierarchicalConfiguration> listItems = config.configurationsAt("list.li");
		for (HierarchicalConfiguration li : listItems) {
			BigDecimal price = BigDecimal.ZERO;
			System.out.println(li.getString("[@price]"));
			String priceField = map.get(li.getString("[@price]"));
			String text = null;
			String name = li.getString("[@name]");
			if (priceField != null) {
				price = new BigDecimal(priceField);
			}
			boolean without = false;
			String field = li.getString("[@without]");
			String s = map.get(field);
			if (s != null && s.length() > 0) {
				without = true;
			}
			boolean inclusive = false;
			field = li.getString("[@inclusive]");
			s = map.get(field);
			if (s != null && s.length() > 0) {
				inclusive = true;
			}

			HierarchicalConfiguration textConf = li.configurationAt("text");
			String para = map.get(name);

			if (without)
				text = textConf.getString("without");
			else if (inclusive)
				text = textConf.getString("inclusive");
			else
				text = textConf.getString("text");

			text = text.trim();

			if (price.compareTo(BigDecimal.ZERO) <= 0 && !without && !inclusive)
				continue;

			for (HierarchicalConfiguration varConf : li.configurationsAt("var")) {
				String value = map.get(varConf.getString("[@name]"));
				if (value == null) {
					continue;
				}
				String token = varConf.getString("[@token]");
				if (token == null) {
					continue;
				}

				if (text.contains(token)) {
					text = text.replaceFirst(token, value);
				}
			}

			String itm;
			if (inclusive)
				itm = text + "\t" + "inklusive";
			else
				itm = text + "\t" + df.format(price) + " " + EURO_SIGN;

			nettoSum = nettoSum.add(price);
			result.add(itm);
		}

		return result;
	}

	private String getText(Map<String, String> map) {

		String s = config.getString("text");
		List<HierarchicalConfiguration> specialVals = config.configurationsAt("special");

		for (HierarchicalConfiguration c : config.configurationsAt("field")) {
			String val = c.getString(".");
			String para = map.get(val);
			String token = c.getString("[@token]");
			String repl = c.getString("[@replace]");
			String nullVal = c.getString("[@nullval]");
			boolean special = false;
			if (para == null) {
				para = nullVal;
				special = true;
			}

			Optional<HierarchicalConfiguration> specialValConfig = specialVals.stream()
					.filter(cnf -> token.equals(cnf.getString("[@for]"))).findAny();
			if (specialValConfig.isPresent()) {
				for (HierarchicalConfiguration svc : specialValConfig.get().configurationsAt("s")) {
					if (para.equalsIgnoreCase(svc.getString("val"))) {
						para = svc.getString("repl");
						special = true;
						break;
					}
				}
			}

			if (para != null) {
				if (!special && repl != null) {
					para = repl.replace("$$", para);
				}
				s = s.replace(token, para);
			}
		}

		return s;
	}

	private String getSubjectLine(Map<String, String> map) {
		String cityF = map.get("from_city");
		String cityT = map.get("to_city");

		String subj = "Angebot: Umzug ";

		if (cityF.equals(cityT)) {
			subj += "innerhalb von " + cityF.trim();
		} else {
			subj += "von " + cityF.trim() + " nach " + cityT.trim();
		}

		return subj;
	}

	private void setPageFormat(TextDocument odt) {
		StyleMasterPageElement page = odt.getOfficeMasterStyles().getMasterPages().next();
		String pageLayoutName = page.getStylePageLayoutNameAttribute();
		OdfStylePageLayout pageLayoutStyle = page.getAutomaticStyles().getPageLayout(pageLayoutName);
		PageLayoutProperties pageLayoutProps = PageLayoutProperties.getOrCreatePageLayoutProperties(pageLayoutStyle);
		// 20 mm seitenabstand
		pageLayoutProps.setMarginLeft(15);
		pageLayoutProps.setMarginTop(8);
		pageLayoutProps.setMarginBottom(8);
		pageLayoutProps.setMarginRight(15);

		pageLayoutProps.setFootnoteMaxHeight(0);
	}

	private String getSalutation(Map<String, String> map, boolean full) {
		String sex = map.get("sex") == null ? sex = "M" : map.get("sex");
		String lname = map.get("lstName");
		String fname = map.get("fstName");

		String name;
		switch (sex.toCharArray()[0]) {
		default:
		case 'M':
			name = "Sehr geehrter Herr ";
			break;
		case 'F':
			name = "Sehr geehrte Frau ";
			break;
		case 'C':
			return "Sehr geehrte Damen und Herren";
		}

		if (full && fname != null) {
			name += fname + " " + lname;
		} else {
			name += lname;
		}

		return name + ",";

	}

	private List<String> getFROMAddress(Map<String, String> map) {
		String company = map.get("company");
		String sex = map.get("sex") == null ? sex = "M" : map.get("sex");
		String lname = map.get("lstName");
		String fname = map.get("fstName");
		String street = map.get("from_street");
		String zip = map.get("from_zip");
		String city = map.get("from_city");

		if (fname != null) {
			lname = fname + " " + lname;
		}

		List<String> lst = new ArrayList<>();
		if (company != null) {
			lst.add("Firma " + company);
		}
		String name = "";
		switch (sex.toCharArray()[0]) {
		default:
		case 'M':
			name = "Herr " + lname;
			break;
		case 'F':
			name = "Frau " + lname;
			break;
		case 'C':
			if (company == null) {
				name = "Frima " + lname;
			}
			break;

		}

		lst.add(name);
		lst.add(street);
		lst.add(zip + " " + city);
		return lst;
	}

	@Override
	public void configurationChanged(ConfigurationEvent event) {
		config.setDelimiterParsingDisabled(true);
	}

	private Text replaceTabs(OdfFileDom dom, Text text) {
		Node parent = text.getParentNode();
		Text cur = text;
		int i;

		while ((i = cur.getTextContent().indexOf('\t')) >= 0) {
			Text next = cur.splitText(i);
			next.setTextContent(next.getTextContent().substring(1));

			TextTabElement tab = (TextTabElement) OdfXMLFactory.newOdfElement(dom, TextTabElement.ELEMENT_NAME);
			parent.insertBefore(tab, next);
			cur = next;
		}

		return cur;
	}
}
