<%@page import="de.marcusthiel.simon.odfgen.db.data.SuccessfullCall"%>
<%@page language="java" contentType="text/html; charset=UTF8"%>
<%@page import="java.sql.*,java.util.*"%>
<%@page import="de.marcusthiel.simon.odfgen.db.*"%>
<%@page import="org.apache.commons.dbutils.QueryRunner"%>
<%!public String getOld(String key, Map map) {
		if (map.containsKey(key)) {
			return map.get(key).toString();
		}
		return "";
	}%>
<%
Map<String, String> map = (Map<String, String>) request.getAttribute("loadData");

if (map == null)
	map = new HashMap<>();
%>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8" />
<title>ODT-Bauer</title>
<script type="text/javascript">
	function addSpecialRow(){
		var table = document.getElementById("special_postions");
		var rowCnt = table.getElementsByTagName("tr").length;
		var row = table.insertRow(rowCnt-1);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		cell1.innerHTML = "<input name='special_name"+rowCnt+"'  placeholder='Bezeichnung'/></td>";
		cell2.innerHTML = "<input type='number' step='0.01' name='special_value"+rowCnt+"'placeholder='Kosten' size='5'/></td>";
		return false;
	}
	function reloadWithData(){
		var select = document.getElementById('oldSelect');
		var idx  = select.selectedIndex;
		var id = select.options[idx].value;
		var url = window.location.href.split('?')[0];
		url += '?oldData='+id
		location.href = url;
	}
	</script>
</head>
<body>
	<form method="post" action="servlet/work/" accept-charset="UTF-8"
		if="form">

		<fieldset
			style="width: 450px; background-color: #b3ffb3; float: left;">
			<legend style="padding: 20px; text-align: center">Aufladeadresse</legend>
			<table border="0" cellspacing="0">
				<tr>
					<td colspan="3"><input name="company" size="35"
						placeholder="Firmenname" autocomplete="off"
						value="<%out.write(getOld("company", map));%>" /></td>
				</tr>
				<tr>
					<td colspan="2">Anrede <select name="sex">
							<option value="M">Herr</option>
							<option value="F">Frau</option>
							<option value="C">Firma</option>
					</select></td>
				</tr>
				<tr>
					<td width="200"><input name="fstName" size="15"
						placeholder="Vorname" autocomplete="off"
						value="<%out.write(getOld("fstName", map));%>" /></td>
					<td><input name="lstName" size="15" placeholder="Nachname"
						autocomplete="off" required
						value="<%out.write(getOld("lstName", map));%>" /></td>
				</tr>
				<tr>
					<td colspan="2"><input name="from_street" size="30"
						placeholder="Straße mit Hausnummer" autocomplete="off" required
						value="<%out.write(getOld("from_street", map));%>" /></td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td><input name="from_zip" size="5" placeholder="Plz"
									autocomplete="off" required
									value="<%out.write(getOld("from_zip", map));%>" /></td>
								<td><input name="from_city" size="28" placeholder="Ort"
									autocomplete="off" required
									value="<%out.write(getOld("from_city", map));%>" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="200">Halteverbot</td>
					<td><input name="from_park_price" type="number" size="1"
						step="0.01" placeholder="EUR" style="width: 70px;"
						autocomplete="off"
						value="<%out.write(getOld("from_park_price", map));%>" /></td>
				</tr>
				<tr id="from_floor_tr">
					<td>Stockwerk</td>
					<td><select name="from_floor" id="from_floor_sel">
							<option value=""
								<%if (getOld("from_floor", map).isEmpty())
	out.write("selected");%>>(
								Haus )</option>
							<option value="Untergeschoss"
								<%if (getOld("from_floor", map).startsWith("U"))
	out.write("selected");%>>Keller</option>
							<option value="Erdgeschoss"
								<%if (getOld("from_floor", map).startsWith("E"))
	out.write("selected");%>>Erdgeschoss</option>
							<option value="1. OG"
								<%if (getOld("from_floor", map).startsWith("1"))
	out.write("selected");%>>1.
								OG</option>
							<option value="2. OG"
								<%if (getOld("from_floor", map).startsWith("2"))
	out.write("selected");%>>2.
								OG</option>
							<option value="3. OG"
								<%if (getOld("from_floor", map).startsWith("3"))
	out.write("selected");%>>3.
								OG</option>
							<option value="4. OG"
								<%if (getOld("from_floor", map).startsWith("4"))
	out.write("selected");%>>4.
								OG</option>
							<option value="5. OG"
								<%if (getOld("from_floor", map).startsWith("5"))
	out.write("selected");%>>5.
								OG</option>
							<option value="6. OG"
								<%if (getOld("from_floor", map).startsWith("6"))
	out.write("selected");%>>6.
								OG</option>
					</select>
				</tr>
			</table>
		</fieldset>

		<fieldset
			style="width: 450px; background-color: #99bbff; float: left;">
			<legend style="padding: 20px; text-align: center">Abladeadresse</legend>
			<table border="0" cellspacing="0">
				<tr>
					<td colspan="2"><input name="to_street" size="30"
						placeholder="Straße mit Hausnummer" autocomplete="off" required
						value="<%out.write(getOld("to_street", map));%>" /></td>
				</tr>
				<tr>
					<td colspan="2">
						<table>
							<tr>
								<td><input name="to_zip" size="5" placeholder="Plz"
									autocomplete="off" required
									value="<%out.write(getOld("to_zip", map));%>" /></td>
								<td><input name="to_city" size="28" placeholder="Ort"
									autocomplete="off" required
									value="<%out.write(getOld("to_city", map));%>" /></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td width="200">Halteverbot</td>
					<td><input name="to_park_price" type="number" size="1"
						step="0.01" placeholder="EUR" style="width: 70px;"
						value="<%out.write(getOld("to_park_price", map));%>" /></td>
				</tr>
				<tr id="to_floor_tr">
					<td>Stockwerk</td>
					<td><select name="to_floor" id="to_floor_sel">
							<option value=""
								<%if (getOld("to_floor", map).isEmpty())
											out.write("selected");%>>( Haus )</option>
							<option value="Untergeschoss"
								<%if (getOld("to_floor", map).startsWith("U"))
	out.write("selected");%>>Keller</option>
							<option value="Erdgeschoss"
								<%if (getOld("to_floor", map).startsWith("E"))
	out.write("selected");%>>Erdgeschoss</option>
							<option value="1. OG"
								<%if (getOld("to_floor", map).startsWith("1"))
	out.write("selected");%>>1.
								OG</option>
							<option value="2. OG"
								<%if (getOld("to_floor", map).startsWith("2"))
	out.write("selected");%>>2.
								OG</option>
							<option value="3. OG"
								<%if (getOld("to_floor", map).startsWith("3"))
	out.write("selected");%>>3.
								OG</option>
							<option value="4. OG"
								<%if (getOld("to_floor", map).startsWith("4"))
	out.write("selected");%>>4.
								OG</option>
							<option value="5. OG"
								<%if (getOld("to_floor", map).startsWith("5"))
	out.write("selected");%>>5.
								OG</option>
							<option value="6. OG"
								<%if (getOld("to_floor", map).startsWith("6"))
	out.write("selected");%>>6.
								OG</option>
					</select>
				</tr>
			</table>
		</fieldset>

		<fieldset
			style="width: 450px; text-align: center; background-color: #b8b894; float: left; clear: left">
			<legend style="padding: 20px; text-align: center">
				Transportkosten (Möbel)</legend>
			<input type="number" name="carry_price" step="0.01"
				placeholder="Kosten" required />
		</fieldset>

		<fieldset
			style="width: 450px; text-align: center; background-color: #b8b894; float: left;">
			<legend style="padding: 20px; text-align: center">Kleiderboxen</legend>

			<input type="checkbox" name="Kleid_without" value="1" id="Kleid_without" />
			<label for="Kleid_without">ohne</label>

			<input type="checkbox" name="Kleid_Inkl" value="1" id="Kleid_Inkl" />
			<label for="Kleid_Inkl">Inklusive</label><br />
			<input type="number"
				name="kleid_count" placeholder="Anzahl" min=0 size="1" /> <input
				type="number" step="0.01" placeholder="Kosten" name="price_kleid"
				min="0" value="<%out.write(getOld("kleid_count", map));%>" />
		</fieldset>

		<fieldset
			style="width: 450px; text-align: center; background-color: #b8b894; float: left; clear: left">
			<legend style="padding: 20px; text-align: center"> Transport
				der Kartons und Packst&uuml;cke</legend>
			<input type="checkbox" name="kartons_without" value="1"
				id="kartons_without" /> <label for="kartons_without">ohne</label> <input
				type="checkbox" name="kartons_Inkl" value="1" id="kartons_Inkl" />
			<label for="kartons_Inkl">Inklusive</label><br /> <input
				type="number" id="num" min=0 name="kart_count" size="1"
				placeholder="Packstücke" /> <input type="number" step="0.01"
				placeholder="Kosten" name="price_karton" min="0" />
		</fieldset>

		<fieldset
			style="width: 450px; text-align: center; background-color: #b8b894; float: left;">
			<legend style="padding: 20px; text-align: center"> Ab- &amp;
				Aufbau der M&ouml;bel</legend>
			<input type="checkbox" name="montage_without" value="1"
				id="montage_without" /> <label for="montage_without">ohne</label> <input
				type="checkbox" name="montage_Inkl" value="1" id="montage_Inkl" />
			<label for="montage_Inkl">Inklusive</label><br /> <input
				type="number" name="montage_price" step="0.01" placeholder="Kosten"
				min=0 />
		</fieldset>



		<fieldset
			style="width: 450px; text-align: center; background-color: #ff9495; float: left; clear: left">
			<legend style="padding: 20px; text-align: center">Einpacken
				inkl. Verpackung</legend>
			<input type="checkbox" name="packIn_without" value="1"
				id="packIn_without" /> <label for="packIn_without">ohne</label> <br />

			<input type="number" step="0.01" placeholder="Kosten"
				name="packIn_price" min="0" />
		</fieldset>

		<fieldset
			style="width: 450px; text-align: center; background-color: #ff9495; float: left;">
			<legend style="padding: 20px; text-align: center">Sonderposten</legend>
			<table id="special_postions">
				<tr>
					<td><input name="special_name1" placeholder='Bezeichnung' /></td>
					<td><input type="number" step="0.01" name="special_value1"
						placeholder='Kosten' size="5" /></td>
				</tr>
				<tr>
					<td><button onclick="addSpecialRow(); return false;">mehr..</button></td>
				</tr>
			</table>
		</fieldset>
		<fieldset style="visibility: hidden;">
			<input name='zubehor' value='0' /> <input name='zubehor_without'
				value="1" />
		</fieldset>
		<br clear="all" />

		<button
			style="width: 480px; height: 120px; font-size: 35pt; float: left;"
			type="submit">
			<b>Los</b>
		</button>
		<button style="width: 480px; height: 120px; font-size: 35pt"
			type="reset">
			<b>L&ouml;schen</b>
		</button>

	</form>
	<br />
	<br />
	<select size="10" style="min-width: 300px;" id="oldSelect">
		<%
		List<Map.Entry<Long, String>> oldmap = (List<Map.Entry<Long, String>>) request.getAttribute("callData");

		for (Map.Entry<Long, String> e : oldmap) {
			out.write("<option value='" + e.getKey() + "'>" + e.getValue() + "</option>\r\n");
		}
		%>
	</select>
	<button onClick="reloadWithData()">Laden mit alten Daten</button>

</body>
</html>
